# OHD Audio Rebuild

This is a sound mod for Operation HarshDoorstop
To use, place into /Mods/ folder in the game as instructed to do for other mods in the documents for OHD

---


Audio Programmer: Rob W.
Audio Engineer: Richard N.
Sound Designer: Richard N.

---

Early Development Stages

# Audio Needs/Wants

Spatial Audio & Reverb Plugin (Builtin or Not)

---

# Current Features In Development

Audio Ducking Mix - for usage in proximity to audio that generally makes other sounds quieter in the world space

Character Injured Mix Effects - for usage when a player is taking damage, or has taken damage

Footsteps & Movement - for usage during character movement

Character Breathing - Hold for sniping, and breathing during medical emergency and stamina loss

Weap: MK18 Sup & UnSup for testing mix and environment effects

Gear Foley - Plate Carrier

Radio Sounds - for radio communications in game

---

# Current Feature Implementations

Sonic Crack Audio Mix

UI Sound Effects

Character Revive BacktoLife Audio Effect - for usage when player is being revived, only for use on the player being revived

Character Heartbeat - 60-100 BPM loops for usage with medical system and stamina system
